# Jellybeams

### Sublime Text Color Scheme

Colorful, dark color scheme based on the excellent Jellybeans color scheme for Vim.

![jellybeams.png](https://bitbucket.org/repo/MRG85d/images/2118417706-jellybeams.png)